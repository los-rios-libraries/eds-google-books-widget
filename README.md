Note: This is no longer updated - it is part of the EDS-Complete repository.

# EDS Google Books Widget
This is inserted in a Detailed Records Widget. It does the following things:

1. loads the Google Books API script
2. if an OCLC number or ISBN is found, it searches Google Books for a match. Note that it has to become visible at this point so that if a match is found, the book viewer can size itself appropriately.
3. if a match is found, it will display. If it is not found, the widget area will hide itself.

You need to have the following in your custom CSS: `#GoogleBooksPreview + div {display:none;}` (assuming GoogleBooksPreview is the name of your detailed record widget)

It is adapted from the script at the [EDS Wiki](http://edswiki.ebscohost.com/Google_Book_Preview) (login required).